//write a program to print all input lines that are longer than 80 characters.

#include <stdio.h>

#define MAXLENGTH 1000
#define PRINTLENGTH 80

main(){
	int c;
	int line_count;
	char printed_line [MAXLENGTH];
	while((c = getchar()) != EOF ){
		for(line_count = 0; c != '\n'; ++line_count){
			printed_line[line_count] = c;
			c = getchar();
		}
		if (line_count > PRINTLENGTH )
			printf("%s\n", printed_line);
	}
}
